@extends('layout')

@section('content')
    <h5>
    @if($product->is_best_seller)
        <span class="badge badge-primary" >BEST SELLER</span>
    @endif
    </h5>
    <h3>{{ $product->name }}</h3>
    <h5 class="text-muted">{{ $product->sku }}</h5>
    <p>{{ $product->description }}</p>
@endsection
