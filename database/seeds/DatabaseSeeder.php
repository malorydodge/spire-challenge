<?php

use App\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create some products
        $products = app(Product::class)->newCollection();

        foreach (range(0, 14) as $i) {
            $products->push(
                factory(Product::class)->create([
                    'name' => 'Spire Product ' . ($i+1),
                    'sku' => 'SPIRE-' . ($i+1),
                ])
            );
        }

        $products->random(10)->each(function ($product) {
            $product->sku .= '-FEATURED'; 

            $product->is_featured = true;
            
            $product->save();
        });

        $products->random(7)->each(function ($product) {
            $product->sku .= '-BEST';

            $product->is_best_seller = true;

            $product->save();
        });
    }
}
