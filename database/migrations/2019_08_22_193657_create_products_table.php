<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('sku');
            $table->text('description');
            $table->decimal('price', 10, 4);
            $table->boolean('is_best_seller');
            $table->boolean('is_featured');
            $table->string('image_url');
            $table->timestamps();
        });
    }
}
