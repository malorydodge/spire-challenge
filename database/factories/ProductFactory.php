<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'price' => $faker->randomFloat(4, 1, 40),
        'image_url' => $faker->imageUrl(500, 300),
        'is_featured' => false,
        'is_best_seller' => false,
    ];
});
