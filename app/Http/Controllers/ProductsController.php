<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('products.list', ['products' => $products]);
    }
    
    public function featured()
    {
        $featuredProducts = Product::where('is_featured', 1)->get();
        return view('products.list', ['products' => $featuredProducts]);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        
        return view('products.show', ['product' => $product]);
    }
}
